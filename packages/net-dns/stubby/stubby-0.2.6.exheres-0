# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=getdnsapi tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require systemd-service [ systemd_files=[ "${WORK}"/systemd/stubby.service ] \
    systemd_tmpfiles=[ "${WORK}"/systemd/stubby.conf ] ]

SUMMARY="local DNS Privacy stub resolver using getdns"
DESCRIPTION="
    Stubby is the name given to a mode of using getdns which enables it to act
    as a local DNS Privacy stub resolver (using DNS-over-TLS).
"

HOMEPAGE="https://getdnsapi.net/blog/dns-privacy-daemon-stubby"
LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"

MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libyaml
        group/stubby
        net-dns/getdns[>=1.5.0]
        user/stubby
"

# Doesn't build with --foreign
AM_OPTS="--gnu"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-piddir=/run
)

src_install() {
    default

    install_systemd_files

    # Tries to install empty piddir
    edo rmdir "${IMAGE}"/run
}

