# Copyright 2017-2018 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

# In theory this is supposed to work with python 3.x too,
# but in practise it doesn't for me :/
require python [ blacklist=3 multibuild=false ]

SUMMARY="Python bindings for the Nautilus file manager"
LICENCES="GPL-2"

PLATFORMS="~amd64 ~x86"
SLOT="0"

MYOPTIONS="gtk-doc"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        gnome-bindings/pygobject:=[python_abis:*(-)?]
        gnome-desktop/nautilus[>=2.32][gobject-introspection(+)]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
)

src_prepare() {
    # otherwise tries to call pkg-config directly
    edo sed "s/pkg-config/${PKG_CONFIG}/" -i configure.ac

    autotools_src_prepare
}

